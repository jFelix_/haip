var app = angular.module('haip', []);
app.controller("mainController", function($scope, $http) {
	$scope.formFields = [];
	$scope.isCall = false;
	$scope.isCall4 = true;
	$scope.isCall5 = true;
	$scope.isCall6 = true;
	$scope.url = 'http://haip.cl/api/challenge';
	$scope.callHaip = function(){
		$http.post('/api/challenge/options', {url: $scope.url})
		.then(function(response) {
			$scope.isCall = true;
			$scope.haip = response.data;
			var key = Object.keys($scope.haip)[0];
			value = $scope.haip[key];
			$http.post('/api/challenge', JSON.stringify(value.body)).then(function(responsePost) {
				$scope.isCall4 = false;
			}, 
			function(data) {
				console.log('Error: ' + data);
			});
		}, 
		function(data) {
			console.log('Error: ' + data);
		});
	};
	$scope.callHaip4 = function(){
		$http.get('/api/challenge')
		.then(function(response) {
			$scope.isCall4 = true;
			$scope.isCall5 = false;
		}, 
		function(data) {
			console.log('Error: ' + data);
		});
	};
	$scope.callHaip5 = function(){
		if ($scope.text) {
			$http.post('/api/challenge/random', {lenght: $scope.text})
			.then(function(response) {
				$scope.permutar = response.data;
				$scope.isCall5 = true;
				$scope.isCall6 = false;
			}, 
			function(data) {
				console.log('Error: ' + data);
			});
		}
	};
	$scope.callHaip6 = function(){
		if ($scope.permutar) {
			$http.post('/api/challenge/permutar', {permutar: $scope.permutar})
			.then(function(response) {
				$scope.responsePermutacion = response.data;
			}, 
			function(data) {
				console.log('Error: ' + data);
			});
		}
	};
});