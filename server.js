import fetch from 'node-fetch';
import express from 'express';
import http from 'http';
import Request from 'request';
import bodyParser from 'body-parser';
import Combinatorics from 'js-combinatorics';
let app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.post('/api/challenge/options', async function(req, res) {
	let api = await fetch(req.body.url,{
		method: 'OPTIONS',
		headers: {
			'Content-Type': 'application/json'
		}
	})
	.then(res => res.json())
	.then(json => {return json} )
	.catch(function(err) {
		console.error(err);
	});
	res.status(200).send(api);
	
});
app.post('/api/challenge', async (req, res)  =>  {	
	let api = await fetch('http://haip.cl/api/challenge',{
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify(req.body)
	})
	.then(res => res.json())
	.then(json => {return json} )
	.catch(function(err) {
		console.error(err);
	});
	res.status(200).send(api);
});
app.get('/api/challenge', async (req, res) =>{
	let api = await fetch('http://haip.cl/api/challenge/generate-test',{
		method: 'GET',
		headers: {
			'Content-Type': 'application/json',
			'haip-test': "haip-OK"
		},
	})
	.then(res => res.json())
	.then(json => {return json} )
	.catch(function(err) {
		console.error(err);
	});
	res.status(200).send(api);
});
app.post('/api/challenge/random', async (req, res) => {
	let length = req.body.lenght;
	var letters = ["x", "*"];
	let response = "";
	for (var i = 0; i < length; i++) {
		response += letters[Math.floor(Math.random()*letters.length)];
	}
	res.status(200).send(response);
});
app.post('/api/challenge/permutar', async (req, res) => {
	let permutar = req.body.permutar;
	let permutacion = [0,1];
	let permutaciones = [];
	let resp = [];
	let posicion = permutar.indexOf("x");
	let cuenta=0;
	while ( posicion != -1 ) {
		cuenta++;
		posicion = permutar.indexOf("x",posicion+1);
	}
	let fact = rFact(cuenta);
	for (var i = 0; i < cuenta; i++) {
		permutaciones.push(permutacion);
	}
	let cp = cartesianProduct(permutaciones);
	for (var i = 0; i < cp.length; i++) {
		var reemplazada = permutar;
		var contar = 0;
		for (var j = 0; j < permutar.length; j++) {
			if(permutar[j] == "x"){
				reemplazada = replaceAt(reemplazada,j,cp[i][contar]);
				contar++;
			}
		}
		resp.push(reemplazada);
	}
	res.status(200).send(resp);
});
function cartesianProduct(arr) {
  return arr.reduce((a, b) =>
    a.map(x => b.map(y => x.concat(y)))
    .reduce((a, b) => a.concat(b), []), [[]]);
}

function rFact(num){
	if (num === 0)
		return 1;
	else
		return num * rFact( num - 1 );
}

function replaceAt(string, index, replace) {
	return string.substring(0, index) + replace + string.substring(index + 1);
}

// Escucha en el puerto 8080 y corre el server
app.listen(8080, function() {
	console.log('App listening on port 8080');
});
app.get('/', function(req, res) {
	res.sendfile('./public/index.html');
});
app.get('/main.js', function(req, res) {
	res.sendfile('./public/main.js');
});